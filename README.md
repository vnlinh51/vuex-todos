# todo-x-app

- Vue 2
- VueX 3.6
- Axios 
- Mockapi : https://62ea4eb93a5f1572e878b7d1.mockapi.io/api/todos

.
.
.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
