import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth'
import todosModules from './modules/todos'



Vue.use(Vuex);

const storeData = {
    modules: {
        auth,
        todosModules
    }
}

const store = new Vuex.Store(storeData);

export default store;
