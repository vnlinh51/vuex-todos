import axios from 'axios';

const todosModules = {
    state: {
        todos: [],
    },

    getters: {
        todos: state => state.todos,
        doneTodos: (state) => state.todos.filter((todo) => todo.completed),
        reverseTodos:state => state.todos.slice().reverse(),
        progress: (state, getters) => {
            const doneTodos = getters.doneTodos;
            return Math.round((doneTodos.length / state.todos.length) * 100);
        },
    },

    actions: {
        //context > context.commit
        async getTodos({ commit }) {
            try {
                const response = await axios.get(
                    'https://62ea4eb93a5f1572e878b7d1.mockapi.io/api/todos'
                );
                commit('SET_TODOS', response.data);
            } catch (error) {
                console.log('Error get todos, code: ', error);
            }
        },

        async addTodo({ commit }, newTodo) {
            try {
                await axios.post(`https://62ea4eb93a5f1572e878b7d1.mockapi.io/api/todos`, newTodo);
                commit('ADD_TODO', newTodo);
            } catch (error) {
                console.log('fail to add new todo: ', error);
            }
        },

        async deleteTodo({ commit }, todoId) {
            try {
                await axios.delete(
                    `https://62ea4eb93a5f1572e878b7d1.mockapi.io/api/todos/${todoId}`
                );
                commit('DELETE_TODO', todoId);
            } catch (error) {
                console.log('Fail to delete: ', error);
            }
        },
    },

    mutations: {
        MARK_COMPLETE(state, todoId) {
            state.todos.map((todo) => {
                if (todo.id === todoId) {
                    todo.completed = !todo.completed;
                    
                }
                return todo;
            });
        },

        DELETE_TODO(state, todoId) {
            state.todos = state.todos.filter((todo) => todo.id !== todoId);
        },

        ADD_TODO(state, newTodo) {
            state.todos.push(newTodo);
        },

        SET_TODOS(state, todos) {
            state.todos = todos;
        },
    },
};

console.log(todosModules)

export default todosModules;
